package com.semebapp.transactionservice.client;

import com.google.common.net.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.Collections;

@Service
public class CurrencyClient {

    private WebClient client;

    public CurrencyClient() {
        initWebClient();
    }

    private void initWebClient() {
        client = WebClient.builder()
                .baseUrl("http://currency-service:5000")
                .defaultCookie("cookieKey", "cookieValue")
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .defaultUriVariables(Collections.singletonMap("url", "http://currency-service:5000"))
                .build();
    }

    public Double convertFromTo(String from, String to, Double amount) {
        Mono<Double> currencyMono = client.get()
                .uri(String.format("/currency/convert/%s/%s/amount/%s",from, to, amount))
                .retrieve()
                .bodyToMono(Double.class);
        return currencyMono.block();
    }
}
