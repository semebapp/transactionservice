package com.semebapp.transactionservice.client;

import com.google.common.net.HttpHeaders;
import com.semebapp.transactionservice.entities.Preferences;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.Collections;

@Service
public class PreferencesClient {

    private WebClient client;

    public PreferencesClient() {
        initWebClient();
    }

    private void initWebClient() {
        client = WebClient.builder()
                .baseUrl("http://preference-service:8080")
                .defaultCookie("cookieKey", "cookieValue")
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .defaultUriVariables(Collections.singletonMap("url", "http://preference-service:8080"))
                .build();
    }

    public Preferences getPreferences(String user) {
        Mono<Preferences> preferencesMono = client.get()
                .uri("/preferences/" + user)
                .retrieve()
                .bodyToMono(Preferences.class);
        return preferencesMono.block();
    }

    public Preferences setPreferences(Preferences preferences) {
        Mono<Preferences> preferencesMono = client.post()
                .uri("/preferences")
                .body(Mono.just(preferences), Preferences.class)
                .retrieve()
                .bodyToMono(Preferences.class);
        return preferencesMono.block();
    }
}
