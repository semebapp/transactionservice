package com.semebapp.transactionservice.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("transactionservice")
public class HealthController {

    @GetMapping("/health")
    public @ResponseBody
    ResponseEntity<String> health() {
        return ResponseEntity.ok("OK");
    }
}
