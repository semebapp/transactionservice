package com.semebapp.transactionservice.dao;

import com.semebapp.transactionservice.entities.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TransactionRepository extends JpaRepository<Transaction, Integer> {

    @Query(value = "SELECT * FROM transaction WHERE from_account = ?1 OR to_account =?1", nativeQuery = true)
    List<Transaction> findByEmailAddress(String account);
}
