package com.semebapp.transactionservice.entities;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "transaction")
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    //@ManyToOne
    private String fromAccount;

    //@ManyToOne
    private String toAccount;

    private Double fromAmount;
    private Double toAmount;
    private LocalDateTime transactionDate;
    private String description;

    public String getFrom() {
        return fromAccount;
    }

    public void setFrom(String from) {
        this.fromAccount = from;
    }

    public String getTo() {
        return toAccount;
    }

    public void setTo(String to) {
        this.toAccount = to;
    }

    public LocalDateTime getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(LocalDateTime transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getFromAmount() {
        return fromAmount;
    }

    public void setFromAmount(Double fromAmount) {
        this.fromAmount = fromAmount;
    }

    public Double getToAmount() {
        return toAmount;
    }

    public void setToAmount(Double toAmount) {
        this.toAmount = toAmount;
    }

    public Integer getId() {
        return id;
    }
}
