package com.semebapp.transactionservice.service;

import com.semebapp.grpc.transactions.*;
import com.semebapp.transactionservice.client.CurrencyClient;
import com.semebapp.transactionservice.client.PreferencesClient;
import com.semebapp.transactionservice.dao.TransactionRepository;
import com.semebapp.transactionservice.entities.Preferences;
import com.semebapp.transactionservice.entities.Transaction;
import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.server.service.GrpcService;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;


@GrpcService
public class TransactionService extends TransactionServiceGrpc.TransactionServiceImplBase {

    @Autowired
    private TransactionRepository repository;

    @Autowired
    private PreferencesClient preferencesClient;

    @Autowired
    private CurrencyClient currencyClient;

    @Override
    public void transact(TransactionCreateRequest request,
                         StreamObserver<TransactionResponse> responseObserver) {
        var transaction = transactionFromGrpc(request);
        var transactionResponse = createFromTransaction(transaction);
        responseObserver.onNext(transactionResponse);
        responseObserver.onCompleted();
    }

    public void getTransactions(TransactionRecieveRequest request,
                                StreamObserver<TransactionList> responseObserver) {
        var transactions = repository.findByEmailAddress(request.getFromAcc());
        var transactionResponseList = new ArrayList<TransactionResponse>();

        for (Transaction transaction : transactions) {
            transactionResponseList.add(createFromTransaction(transaction));
        }
        TransactionList answer = TransactionList.newBuilder()
                .addAllTransaction(transactionResponseList)
                .build();
        responseObserver.onNext(answer);
        responseObserver.onCompleted();
    }


    private Transaction transactionFromGrpc(TransactionCreateRequest request) {
        var transaction = new Transaction();
        transaction.setTransactionDate(LocalDateTime.now());
        transaction.setDescription(request.getDescription());
        transaction.setFrom(request.getFromAcc());
        transaction.setTo(request.getToAcc());
        transaction.setFromAmount(request.getAmount());
        transaction.setToAmount(calculateToAmount(request.getToAcc(), request.getAmount(), request.getFromAcc()));
        return repository.save(transaction);
    }

    private Double calculateToAmount(String toAccount, Double amount, String fromAccount) {
        Preferences fromPreferences = preferencesClient.getPreferences(fromAccount);
        Preferences toPreferences = preferencesClient.getPreferences(toAccount);
        return currencyClient.convertFromTo(fromPreferences.getCurrencyCode(), toPreferences.getCurrencyCode(), amount);
    }

    private TransactionResponse createFromTransaction(Transaction transaction) {
        TransactionResponse.Builder builder = TransactionResponse.newBuilder();
        builder.setDescription(transaction.getDescription());
        builder.setAmountFrom(transaction.getFromAmount());
        builder.setAmountTo(transaction.getToAmount());
        builder.setFromAcc(transaction.getFrom());
        builder.setToAcc(transaction.getTo());
        ZonedDateTime zdt = transaction.getTransactionDate().atZone(ZoneId.of("Europe/Berlin"));
        builder.setTransactionTime(zdt.toInstant().toEpochMilli());
        return builder.build();
    }
}
